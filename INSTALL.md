# Instalación

## Vagrant
~~~sh
apt-get -y install vagrant vagrant-libvirt
ssh-keygen -t rsa -b 4096
ssh-copy-id -i .ssh/id_rsa.pub  operador@10.9.9.9
exit
~~~
### Using libvirt from Vagrant without password prompts

Using `libvirt` provider requires you to type your administrator password every time you create, start, halt or destroy your domains. Fortunately you can avoid this by adding yourself to the `libvirt` group:

```sh
$ sudo gpasswd -a ${USER} libvirt
$ newgrp libvirt
```
## Modificaciones

Para los ajustes es necesario editar:

* `Vagrantfile`

Contiene IP, hostname y configuraciones de los nodos


* `syncFolder/k8s-api.conf`

contine los hostnames de los nodos de control


* `syncFolder/k8s-hosts`

Contiene IP y hostname de todos los nodos


* `syncFolder/keepalived.conf`

Contine la IP del API y el interfaz de red que monitoriza

* `syncFolder/setup_post-install.bash`

Contine las IPs que MetalLB y egress utilizará como puntos de entrada


### Roles
Existen 4 roles:
* aplb --> API Load Balancer
* almc --> Almacenamiento
* ctrl --> Nodos de control-plane
* worker --> Nodos de trabajo

Antes de esplegar los `crtl` hay que confirmar que el LoadBalancer está operativo
    en principio este rol estaba integrado en `almc`ya que unirlo a `ctrl`como se intetó originalmente, daba problemas.
    Finalmente se despliega en dos nodos adiciones

## Despliegue

### API LoadBalancer
Desplegar **aplb**, para que la **API** esté disponible al desplegar los **ctrl**
~~~sh
vagrant up k8s-aplb-01 k8s-aplb-02
nc -v api.k8s 6443
Connection to localhost 6443 port [tcp/*] succeeded!
~~~
Confirmar que podemos acceder a la página <https://api.k8s.cagb.es:6443> y obtenermos un error similar a este:

![API_LB-Error](./Docs/images/API-LB_error.png)

También podemos entrar en estos nodos y confirmar que están arrancados tanto `keepalived` como `haproxy`
~~~sh
systemctl status keepalived
systemctl status haproxy
~~~
![API_LB-Error](./Docs/images/API-LB_ok.png)


### Almacenamiento
Desplegar **almc**, en princpio se trata de una VM con NFS, pero podría ser una NAS o similar
~~~sh
vagrant up k8s-almc-01
~~~

### Control Panel
Desplegar **ctrl**, primero el nodo principal, que dará de alta etcd, api, certficados pki, ...
> Requisito previo tener **aplb** funcionando
~~~sh
vagrant up k8s-ctrl-01
~~~

Una vez completado con éxito, podemos copiar el fichero de configuracion del nodo `/etc/kubernetes/admin.conf` y traerlo a nuestro equipo, normalmente como `~/.kube/config` para poder trabajar con el cluster, con `lens` (<https://k8slens.dev/>), kubectl, ...

A continuacion desplegar el resto, "en teoría" permite paralelismo, pero en las pocas pruebas realizadas, en ocasiones falla alguno:
~~~sh
vagrant up k8s-ctrl-02
vagrant up k8s-ctrl-03
~~~

Desde `ctrl-01` podemos confirmar si está funcionando todo correcto (además de los logs de vagrant en pantalla)
~~~sh
kubectl get nodes
~~~
