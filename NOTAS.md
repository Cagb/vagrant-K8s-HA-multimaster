# Notas
## Vagrant
### Repositorio de imágenes

Imágenes [vagrant Cloud libvirt](https://app.vagrantup.com/boxes/search?utf8=%E2%9C%93&sort=downloads&provider=libvirt&q=debian)

### Configuración con libvirt

Parámetros del plugin [libvirt parameters](https://github.com/vagrant-libvirt/vagrant-libvirt)

Ejemplo de uso:
~~~Vagrantfile
Vagrant.require_plugin "vagrant-libvirt"
  # If you are still using old centos box, you have to setup root username for
  # ssh access. Read more in section 'SSH Access To VM'.
  config.ssh.username = "root"
  
  config.vm.provider :libvirt do |libvirt|
    libvirt.driver = "kvm"
    libvirt.host = "10.9.9.9"
    libvirt.connect_via_ssh = true
    libvirt.username = "operador"
    #libvirt.password = "aBc."
    libvirt.storage_pool_name = "SSD_01"
  end
  
  config.vm.define :dbserver do |dbserver|
    dbserver.vm.box = "debian/testing64"
    test_vm1.vm.network :public_network, :dev => "eth0", :mode => 'bridge'
    dbserver.vm.provider :libvirt do |domain|
      domain.memory = 2048
      domain.cpus = 2
      domain.nested = true
      domain.volume_cache = 'none'
    end
  end
    
~~~

### Uso

~~~sh
vagrant init debian/buster64
export VAGRANT_DEFAULT_PROVIDER=libvirt
vagrant up
vagrant provision
#vagrant up --provider=libvirt
vagrant ssh
~~~


#### Networking

##### privado + IP fija

~~~Vagrantfile
    # Networking Privado, con IP fija para que sea más fácil hacer SSH desde el Host.
    config.vm.network :private_network,
                      :ip => "10.20.30.40",
                      :libvirt__domain_name => "coder.local"
~~~
##### público + IP fija

~~~Vagrantfile
    # Networking Público, con IP fija 
    config.vm.network "public_network",
                      :dev => "br0",
                      :mode => "bridge",
                      :type => "bridge",
                      :ip => "192.168.1.100"
~~~

