# K8s - Vagrant
## Objetivo

Con este proyecto se pretende desplegar mediante **vagrant** los nodos necesarios para crear un cluster multinodo de **Kubernetes**.

La configuracion se realiza mediante roles, y las tareas concreas de cada rol se realizará en un script distinto.

Pendiente realizar la sincronización en el despliegue de nodos, por el momento es necesario desplegarlos en este orden:

1. aplb
2. almc
3. ctrl-01
4. ctrl-*
5. work-*

Además parece que al hacerlo de forma simultanea falla por lo que tanto `4` como `5` es preferible hacerlo de uno en uno.

![esquema](./Docs/images/esquema.png)