﻿# -*- mode: ruby -*-
# vi: set ft=ruby :

# https://github.com/vagrant-libvirt/vagrant-libvirt
## https://docs.vagrantup.com.
### boxes at https://vagrantcloud.com/search.

VAGRANTFILE_API_VERSION = "2"
ENV["LC_ALL"] = "es_ES.UTF-8"

ENV['VAGRANT_DEFAULT_PROVIDER'] = 'hyperv'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  #config.vagrant.plugins = ["hyperv"]
  #config.vagrant.sensitive = ["MySecretPassword", ENV["MY_TOKEN"]]

  K8SDOMAIN="k8s.cagb.es"
  VM=[
     {
        :host => "k8s-aplb-01",
        :tipo => "aplb",
        :num => "01",
        :hostname => "aplb-01",
        :ip => "10.9.9.152",
        :ram => 1024,
        :cpu => 1,
        :rol_aplb => true,
        :rol_almc => false,
        :rol_ctrl => false,
        :rol_work => false
     },
     {
        :host => "k8s-aplb-02",
        :tipo => "aplb",
        :num => "02",
        :hostname => "aplb-02",
        :ip => "10.9.9.153",
        :ram => 1024,
        :cpu => 1,
        :rol_aplb => true,
        :rol_almc => false,
        :rol_ctrl => false,
        :rol_work => false
     },

     {
        :host => "k8s-almc-01",
        :tipo => "almc",
        :num => "01",
        :hostname => "almc-01",
        :ip => "10.9.9.151",
        :ram => 2048,
        :cpu => 2,
        :rol_aplb => false,
        :rol_almc => true,
        :rol_ctrl => false,
        :rol_work => false
     },

     {
        :host => "k8s-ctrl-01",
        :tipo => "ctrl",
        :num => "01",
        :hostname => "ctrl-01",
        :ip => "10.9.9.131",
        :ram => 2048,
        :cpu => 2,
        :rol_aplb => false,
        :rol_almc => false,
        :rol_ctrl => true,
        :rol_work => false
     },
     {
        :host => "k8s-ctrl-02",
        :tipo => "ctrl",
        :num => "02",
        :hostname => "ctrl-02",
        :ip => "10.9.9.132",
        :ram => 2048,
        :cpu => 2,
        :rol_aplb => false,
        :rol_almc => false,
        :rol_ctrl => true,
        :rol_work => false
     },
     {
        :host => "k8s-ctrl-03",
        :tipo => "ctrl",
        :num => "03",
        :hostname => "ctrl-03",
        :ip => "10.9.9.133",
        :ram => 2048,
        :cpu => 2,
        :rol_aplb => false,
        :rol_almc => false,
        :rol_ctrl => true,
        :rol_work => false
     },

     {
        :host => "k8s-work-01",
        :tipo => "work",
        :num => "01",
        :hostname => "work-01",
        :ip => "10.9.9.141",
        :ram => 3072,
        :cpu => 2,
        :rol_aplb => false,
        :rol_almc => false,
        :rol_ctrl => false,
        :rol_work => true
     },
     {
        :host => "k8s-work-02",
        :tipo => "work",
        :num => "02",
        :hostname => "work-02",
        :ip => "10.9.9.142",
        :ram => 3072,
        :cpu => 2,
        :rol_aplb => false,
        :rol_almc => false,
        :rol_ctrl => false,
        :rol_work => true
     },
     {
        :host => "k8s-work-03",
        :tipo => "work",
        :num => "03",
        :hostname => "work-03",
        :ip => "10.9.9.143",
        :ram => 3072,
        :cpu => 2,
        :rol_aplb => false,
        :rol_almc => false,
        :rol_ctrl => false,
        :rol_work => true
     }
  ]

  
  VM.each do |maquina|
      config.vm.define maquina[:host] do |nodo|
         nodo.vm.box = "generic/debian11"
         nodo.vm.network :public_network, :dev => "enp0s25", ip: maquina[:ip], :netmask => "255.255.255.0",
            bridge: "enp0s25", use_dhcp_assigned_default_route: false
         nodo.vm.synced_folder "../syncFolder", "/vagrant", type: "rsync"

         nodo.vm.hostname = maquina[:hostname] + "." + K8SDOMAIN

         nodo.vm.provider :libvirt do |vmp|
            #vmp.title = 'Titulo'
            #vmp.description = 'descripción'
            vmp.default_prefix = 'tf-'
            #vmp.machine_virtual_size = 20
            #vmp.driver = "kvm"
            #vmp.host = "10.9.9.9"
            vmp.connect_via_ssh = true
            #vmp.username = "operador"
            #p.password = "abc."
            #vmp.storage_pool_name = "SSD_01"

            #vmp.loader = '/usr/share/OVMF/OVMF_CODE.fd'
            vmp.autostart = false
            vmp.memory = maquina[:ram]
            vmp.cpus = maquina[:cpu]
            #vmp.cpu_mode = 'custom'
            #vmp.cpu_model = 'Skylake-Server-IBRS'
            #vmp.cpuset = '1-4,^3,6'
            #vmp.cputopology :sockets => '2', :cores => '2', :threads => '1'
            #vmp.cpu_fallback = 'allow'
            #vmp.nested = true
			vmp.enable_virtualization_extensions = true
			vmp.differencing_disk = true 

            #vmp.volume_cache = 'none'
            vmp.disk_driver :cache => 'none'
            
            #vmp.graphics_type = 'spice' # sdl curses none gtk vnc spice
            #vmp.video_type = 'qxl'
            
            #vmp.memballoon_enabled = true
            #vmp.memballoon_model = 'virtio'

            #vmp.storage :file, :size => '20G'
            if maquina[:tipo] == 'almc'
               vmp.storage :file, :cache => 'none', :size => '128G', :bus => 'scsi', :type => 'qcow2', :discard => 'unmap', :detect_zeroes => 'on'
            end
          
         end
       
         nodo.vm.provision "shell", preserve_order: true, path: "../syncFolder/scripts/setup_comun.bash" , args: [ maquina[:host] , maquina[:tipo] , maquina[:num] ]

         if maquina[:rol_aplb] == true
            nodo.vm.provision "shell", preserve_order: true, path: "../syncFolder/scripts/setup_apilb.bash" , args: [ maquina[:host] , maquina[:tipo] , maquina[:num] ]
         end

         if maquina[:rol_almc] == true
            nodo.vm.provision "shell", preserve_order: true, path: "../syncFolder/scripts/setup_almc.bash" , args: [ maquina[:host] , maquina[:tipo] , maquina[:num] ]
         end

         if maquina[:rol_ctrl] == true
            nodo.vm.provision "shell", preserve_order: true, path: "../syncFolder/scripts/setup_comunk8s.bash" , args: [ maquina[:host] , maquina[:tipo] , maquina[:num] ]
            nodo.vm.provision "shell", preserve_order: true, path: "../syncFolder/scripts/setup_ctrl.bash" , args: [ maquina[:host] , maquina[:tipo] , maquina[:num] ]
         end

         if maquina[:rol_work] == true
            nodo.vm.provision "shell", preserve_order: true, path: "../syncFolder/scripts/setup_comunk8s.bash" , args: [ maquina[:host] , maquina[:tipo] , maquina[:num] ]
            nodo.vm.provision "shell", preserve_order: true, path: "../syncFolder/scripts/setup_work.bash" , args: [ maquina[:host] , maquina[:tipo] , maquina[:num] ]
         end
       
      end

   end

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end
