#!/bin/bash

ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "almc-01.k8s"

ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "aplb-01.k8s"
ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "10.9.9.152"
ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "aplb-02.k8s"
ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "10.9.9.153"

ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "ctrl-01.k8s"
ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "ctrl-02.k8s"
ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "ctrl-03.k8s"

ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "work-03.k8s"
ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "work-02.k8s"
ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "work-01.k8s"

