## Generación
La primera vez debe generarse, sin contraseña permite automatización, pero si no se va ha utilizar para ello, mejor utilizar contraseña.

Para no necesitar modificar los scripts, debe guardarse con el nombre `id_k8s-ssh-key` y la llave pública como "`id_k8s-ssh-key.pub`".

~~~sh
ssh-keygen -f ~/.ssh/id_k8s-ssh-key -b 3072 -t rsa -C "Conexion a servidores k8s"
~~~ 

Actualmente recomiendan utilizar Ed25519:
> Hasta 4096 bits la seguridad es equivalente entre ambas, siendo el rendimiento mejor para Ed25519
~~~sh
ssh-keygen -f ~/.ssh/id_k8s-ssh-key -t Ed25519 -C "Conexion a servidores k8s"
~~~ 

Una vez generado, copiarlas a ubicacion correcta:
~~~sh
cp ~/.ssh/id_k8s-ssh-key.pub ./syncFolder
~~~

Para conectar utilizaremos un comando similar a:
~~~bash
ssh ~/.ssh/id_k8s-ssh-key operador@k8s-strg-01.k8s.cagb.es
~~~


Una simplificación puede ser, además, _añadir_ entradas para su utilizacion en el fichero `~/.ssh/config` (comprobar hostnames utilizados):
~~~
# k8s Keys
Host *.k8s.cagb.es
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_k8s-ssh-key
  User operador

~~~

> Si va utilizarse desde más de un equipo, necesitara copiar los tres ficheros al nuevo equipo cliente (**sólo para conectar por ssh a los servidores**)
> * `~/.ssh/config`
> 
> * `~/.ssh/id_k8s-ssh-key`
> 
> * `~/.ssh/id_k8s-ssh-key.pub`

