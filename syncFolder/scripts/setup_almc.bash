#!/bin/bash

[ -z $1 ] && echo "No se ha proporcionado un nombre de nodo" && exit 128
k8s_NODO=$1

[ -z $2 ] && echo "No se ha proporcionado un tipo de nodo" && exit 128
k8s_TIPO=$2

[ -z $3 ] && echo "No se ha proporcionado un numero de nodo" && exit 128
k8s_NUM=$3

. /vagrant/variablesEntorno.bash

#[ ${k8s_NODO} != $H ] && echo "Nombre de nodo incorrecto: $H y se esperaba ${k8s_NODO}" && exit 128
[ ${k8s_TIPO} != $T ] && echo "Tipo de nodo incorrecto: $T y se esperaba ${k8s_TIPO}" && exit 128
[ ${k8s_NUM} -ne $N ] && echo "Numero de nodo incorrecto: $N y se esperaba ${k8s_NUM}" && exit 128


echo "  -->>    -->>  Iniciando ALMACENAMIENTO en $(hostname): $0"
echo

echo "  -->>  Instalando LVM sobre /dev/sda en /datadisk01/"
DEBIAN_FRONTEND=noninteractive apt-get install -y lvm2 btrfs-progs 
pvcreate /dev/sda
vgcreate vgNFS-01 /dev/sda
lvcreate -L15G -n lvNFS-01 vgNFS-01
LV_Path=`lvdisplay | grep 'LV Path' | awk '{print $3}'`
mkfs.btrfs -f -L "NFSvol-01" "${LV_Path}"
mkdir -p /datadisk01 /BASE.datadisk01
mount -t btrfs "${LV_Path}" /datadisk01 -t btrfs
btrfs subvol create /datadisk01/@
umount /datadisk01
echo "${LV_Path}     /datadisk01              btrfs   defaults,discard,subvol=@   0   1" >> /etc/fstab
echo "${LV_Path}     /BASE.datadisk01         btrfs   defaults,discard,subvol=/   0   1" >> /etc/fstab
mkdir -p /datadisk01 /BASE.datadisk01
mount /datadisk01
mount /BASE.datadisk01
echo

echo "  -->>  Instalando NFS sobre el rescurso de /dev/sda en /datadisk01/"
DEBIAN_FRONTEND=noninteractive apt-get install -y nfs-server libnfs-utils nfs4-acl-tools
systemctl enable --now nfs-server
mkdir -p /datadisk01/storage1
echo "/datadisk01/storage1 10.9.9.0/24(rw,sync,no_root_squash,subtree_check)" >> /etc/exports
exportfs -rav
exportfs  -v

echo "  -->>    -->>  Finalizando ALMACENAMIENTO en $(hostname): $0"

