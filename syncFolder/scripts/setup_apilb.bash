#!/bin/bash

[ -z $1 ] && echo "No se ha proporcionado un nombre de nodo" && exit 128
k8s_NODO=$1

[ -z $2 ] && echo "No se ha proporcionado un tipo de nodo" && exit 128
k8s_TIPO=$2

[ -z $3 ] && echo "No se ha proporcionado un numero de nodo" && exit 128
k8s_NUM=$3

. /vagrant/variablesEntorno.bash

#[ ${k8s_NODO} != $H ] && echo "Nombre de nodo incorrecto: $H y se esperaba ${k8s_NODO}" && exit 128
[ ${k8s_TIPO} != $T ] && echo "Tipo de nodo incorrecto: $T y se esperaba ${k8s_TIPO}" && exit 128
[ ${k8s_NUM} -ne $N ] && echo "Numero de nodo incorrecto: $N y se esperaba ${k8s_NUM}" && exit 128


echo "  -->>    -->>  Iniciando LoadBalancer en $(hostname): $0"
echo

echo "  -->>  Instalando HAProxy y keepalived"
DEBIAN_FRONTEND=noninteractive apt-get install -y haproxy keepalived psmisc
systemctl stop keepalived.service
systemctl stop haproxy
echo

echo "  -->>  Configurando keepalived"
echo "net.ipv4.ip_forward = 1" | tee -a /etc/sysctl.conf
sysctl -p

cp /vagrant/keepalived.conf /etc/keepalived/
[ ${k8s_NUM} -ne 01 ] && sed -i 's/state MASTER/state BACKUP/' /etc/keepalived/keepalived.conf
#sed -i "s/priority 250/priority $((250 +51 - $(grep $(hostname) /etc/hosts|grep -v '127.0.1.1' |awk '{print $1}' |cut -d. -f4|cut -c2-3) ))/" /etc/keepalived/keepalived.conf
sed -i "s/priority 250/priority $((100 - $N))/" /etc/keepalived/keepalived.conf
systemctl start keepalived.service
echo

echo "  -->>  Configurando HAproxy"
systemctl stop haproxy
cat >>  /etc/haproxy/haproxy.cfg <<EOF

frontend k8s-apiserver
   bind 10.9.9.130:6443
   mode tcp
   option tcplog
   default_backend k8s-apiserver

backend k8s-apiserver
   mode tcp
   #option tcplog
   option tcp-check
   balance roundrobin
   default-server inter 10s downinter 5s rise 2 fall 2 slowstart 60s maxconn 250 maxqueue 256 weight 100

   server ctrl-01 10.9.9.131:6443 check check-ssl verify none inter 10000 fall 3 rise 2
   server ctrl-02 10.9.9.132:6443 check check-ssl verify none inter 10000 fall 3 rise 2
   server ctrl-03 10.9.9.133:6443 check check-ssl verify none inter 10000 fall 3 rise 2
EOF
echo
echo "  -->>  Cargando HAproxy"
sleep 5s ; systemctl start haproxy

echo "" >> /etc/crontab
echo "@reboot root  sleep 20s && systemctl start keepalived.service ; systemctl start haproxy" "" >> /etc/crontab
echo "" >> /etc/crontab

echo
echo "  -->>    -->>  Finalizando LoadBalancer en $(hostname): $0"
echo
