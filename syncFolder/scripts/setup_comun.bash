#!/bin/bash

[ -z $1 ] && echo "No se ha proporcionado un nombre de nodo" && exit 128
k8s_NODO=$1

[ -z $2 ] && echo "No se ha proporcionado un tipo de nodo" && exit 128
k8s_TIPO=$2

[ -z $3 ] && echo "No se ha proporcionado un numero de nodo" && exit 128
k8s_NUM=$3

. /vagrant/variablesEntorno.bash

#[ ${k8s_NODO} != $H ] && echo "Nombre de nodo incorrecto: $H y se esperaba ${k8s_NODO}" && exit 128
[ ${k8s_TIPO} != $T ] && echo "Tipo de nodo incorrecto: $T y se esperaba ${k8s_TIPO}" && exit 128
[ ${k8s_NUM} -ne $N ] && echo "Numero de nodo incorrecto: $N y se esperaba ${k8s_NUM}" && exit 128


echo "  -->>    -->>  Iniciando COMUN en $(hostname): $0"
echo
echo "  -->>  Creaando usuario ·${K8S_USER}·"
useradd -ms /bin/bash -G sudo ${K8S_USER}
echo "${K8S_USER}:myTest" | chpasswd
chown :k8sadmin /vagrant/
chmod g+rwx /vagrant/
echo
echo "  -->>  Actualizando sistema"
DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get upgrade -y
echo
echo "  -->>  Generando locales ·es_ES.UTF-8 UTF-8·"
DEBIAN_FRONTEND=noninteractive apt-get install -y locales
sed -i -e 's/# es_ES.UTF-8 UTF-8/es_ES.UTF-8 UTF-8/' /etc/locale.gen
dpkg-reconfigure --frontend=noninteractive locales
update-locale LANG=es_ES.UTF-8
export LANG=es_ES.UTF-8 
echo
echo "  -->>  Agregando ips de hosts"
cat /vagrant/k8s-hosts >> /etc/hosts
echo

echo "  -->>  Activando ruta a traves de eth1"
ip route del default
ip route add default via 10.9.9.1 dev eth1
echo

echo "  -->>  Agregando ssh-key a ${K8S_USER}"
mkdir /home/${K8S_USER}/.ssh
cat /vagrant/id_k8s-ssh-key.pub >> /home/${K8S_USER}/.ssh/authorized_keys
cat /vagrant/ssh-config >> /home/${K8S_USER}/.ssh/config
cp /vagrant/id_k8s-ssh-key /home/${K8S_USER}/.ssh/
cp /vagrant/id_k8s-ssh-key.pub /home/${K8S_USER}/.ssh/
chown -R ${K8S_USER}:${K8S_USER} /home/${K8S_USER}/.ssh
echo

echo "  -->>  Agregando ssh-key a root"
mkdir /root/.ssh
cat /vagrant/id_k8s-ssh-key.pub >> /root/.ssh/authorized_keys
cat /vagrant/ssh-config >> /root/.ssh/config
cp /vagrant/id_k8s-ssh-key /root/.ssh/
cp /vagrant/id_k8s-ssh-key.pub /root/.ssh/
chown -R root:root /root/.ssh
echo

echo "  -->>  Desactivando swap"
swapoff -a
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
echo

echo "  -->>  Configurando NTP"
DEBIAN_FRONTEND=noninteractive apt-get install -y  ntp ntpdate
systemctl start ntp 
echo

echo "  -->>    -->>  Finalizando COMUN en $(hostname): $0"
echo


#if [ ${k8s_TIPO} == "almc" ];
#then
#    echo "  -->>  Iniciando LoadBalancer en $(hostname): $0"
#    echo
#    echo "Instalando HAProxy y keepalived"
#    DEBIAN_FRONTEND=noninteractive apt-get install -y haproxy keepalived psmisc
#    echo
#    echo "Configurando keepalived"
#    echo "net.ipv4.ip_forward = 1" | tee -a /etc/sysctl.conf
#    sysctl -p
#    cp /vagrant/keepalived.conf /etc/keepalived/
#    echo
#    [ ${k8s_NUM} -ne 01 ] && sed -i 's/state MASTER/state BACKUP/' /etc/keepalived/keepalived.conf
#    sed -i "s/priority 250/priority $((250 +51 - $(grep $(hostname) /etc/hosts|grep -v '127.0.1.1' |awk '{print $1}' |cut -d. -f4|cut -c2-3) ))/" /etc/keepalived/keepalived.conf
#    systemctl restart keepalived.service
#    echo
#    echo "Configurando HAproxy"
#    systemctl stop haproxy
#    cat >>  /etc/haproxy/haproxy.cfg <<EOF
#
#frontend k8s-apiserver
#   bind 10.9.9.130:6443
#   mode tcp
#   option tcplog
#   default_backend k8s-apiserver
#
#backend k8s-apiserver
#   mode tcp
#   #option tcplog
#   option tcp-check
#   balance roundrobin
#   default-server inter 10s downinter 5s rise 2 fall 2 slowstart 60s maxconn 250 maxqueue 256 weight 100
#
#   server ctrl-01 10.9.9.131:6443 check fall 3 rise 2
#   server ctrl-02 10.9.9.132:6443 check fall 3 rise 2
#   server ctrl-03 10.9.9.133:6443 check fall 3 rise 2
#EOF
#    echo
#    echo "Cargando HAproxy"
#    sleep 5s ; systemctl start haproxy
#    echo
#    echo "  -->>  Finalizando LoadBalancer en $(hostname): $0"
#    echo
#fi


#if [ ${k8s_TIPO} == "almc" ];
#then
#    echo "  -->>  Iniciando ALMACENAMIENTO en $(hostname): $0"
#    echo
#
#    echo "Instalando LVM sobre /dev/sda"
#    DEBIAN_FRONTEND=noninteractive apt-get install -y lvm2 btrfs-progs 
#    pvcreate /dev/sda
#    vgcreate vgNFS-01 /dev/sda
#    lvcreate -L15G -n lvNFS-01 vgNFS-01
#    LV_Path=`lvdisplay | grep 'LV Path' | awk '{print $3}'`
#    mkfs.btrfs -f -L "NFSvol-01" "${LV_Path}"
#    mkdir -p /datadisk01 /BASE.datadisk01
#    mount -t btrfs "${LV_Path}" /datadisk01 -t btrfs
#    btrfs subvol create /datadisk01/@
#    umount /datadisk01
#    echo "${LV_Path}     /datadisk01              btrfs   defaults,discard,subvol=@   0   1" >> /etc/fstab
#    echo "${LV_Path}     /BASE.datadisk01         btrfs   defaults,discard,subvol=/   0   1" >> /etc/fstab
#    mkdir -p /datadisk01 /BASE.datadisk01
#    mount /datadisk01
#    mount /BASE.datadisk01
#    echo

#    echo "Instalando LVM sobre /dev/sda"
#    DEBIAN_FRONTEND=noninteractive apt-get install -y nfs-server libnfs-utils nfs4-acl-tools
#    systemctl enable --now nfs-server
#    mkdir -p /datadisk01/storage1
#    echo "/datadisk01/storage1 10.9.9.0/24(rw,sync,no_root_squash,subtree_check)" >> /etc/exports
#    exportfs -rav
#    exportfs  -v
#
#    echo "  -->>  Finalizando ALMACENAMIENTO en $(hostname): $0"
#echo
#fi


#if [ ${k8s_TIPO} == "ctrl" -o ${k8s_TIPO} == "work" ];
#then
#    echo "  -->>  Iniciando COMUN_K8S en $(hostname): $0"
#    echo
#
#    echo "Instalando herramientas Docker"
#    DEBIAN_FRONTEND=noninteractive apt-get install -y docker.io
#    cat > /etc/docker/daemon.json <<EOF
#{
#    "exec-opts": ["native.cgroupdriver=systemd"],
#    "log-driver": "json-file",
#    "log-opts": {
#        "max-size": "100m"
#    },
#    "storage-driver": "overlay2",
#    "storage-opts": [
#        "overlay2.override_kernel_check=true"
#    ]
#}
#EOF
#    echo
#
#    echo "Instalando Repositorio Kubernetes"
#    DEBIAN_FRONTEND=noninteractive apt-get install -y curl apt-transport-https gnupg
#    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
#    echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list
#    echo
#
#    echo "Instalando kubeadm, kubelet y kubectl"
#    DEBIAN_FRONTEND=noninteractive apt-get update
#    DEBIAN_FRONTEND=noninteractive apt-get install -y kubelet kubeadm kubectl
#    echo
#
#    echo "Letting iptables see bridged traffic"
#    echo "br_netfilter" > /etc/modules-load.d/k8s.conf
#    echo "net.bridge.bridge-nf-call-ip6tables = 1" > /etc/sysctl.d/k8s.conf
#    echo "net.bridge.bridge-nf-call-iptables = 1" >> /etc/sysctl.d/k8s.conf
#    sudo sysctl --system
#    echo
#
#    echo "  -->>  Finalizando COMUN_K8S en $(hostname): $0"
#echo
#fi


if [ ${k8s_TIPO} == "ctrl" ];
then
    echo "  -->>  Iniciando CONTROL en $(hostname): $0"
    echo
    kubeadm config images pull
    if [ ${k8s_NUM} -eq 01 ];
    then
        echo "  -->>    -->>  Principal"
        kubeadm init \
            --control-plane-endpoint "api.k8s.cagb.es:6443" \
            --pod-network-cidr "10.128.0.0/9" \
            --service-dns-domain "apps.k8s.cagb.es" \
            --upload-certs \
            | tee /vagrant/k8s-init.txt
            #--dry-run \
            #--apiserver-bind-port 56443 \
            #--kubernetes-version "1.22.4" \
            #--ignore-preflight-errors=Port-6443 \
            # -v 5
        echo "Generando comandos para JOIN"
        tail -2 /vagrant/k8s-init.txt > /vagrant/add_worker.sh
        tail -12 /vagrant/k8s-init.txt | head -3 > /vagrant/add_ctrl.sh
        touch /vagrant/init.unlock
        echo
     
    else
        echo "  -->>    -->>  Secundario"
        echo "Esperando los scripts de JOIN"
        while [ ! -f /vagrant/init.unlock ];
        do 
            sleep 10s
            scp -o 'StrictHostKeyChecking no' $(grep '## Control panel' /etc/hosts |grep  01| awk '{print $3}'):/vagrant/init.unlock  /vagrant/
            echo -n '.'
        done
        scp -o 'StrictHostKeyChecking no' $(grep '## Control panel' /etc/hosts |grep  01| awk '{print $3}'):/vagrant/add_{ctrl,worker}.sh /vagrant/
        echo "Ejecutando los scripts de JOIN"
        bash /vagrant/add_ctrl.sh
        echo
        
    fi
    echo "Copiando fichero CONFIG al usuario ${K8S_USER}"
    mkdir -p /home/${K8S_USER}/.kube
    cp /etc/kubernetes/admin.conf /home/${K8S_USER}/.kube/config
    chown ${K8S_USER}:${K8S_USER} /home/${K8S_USER}/.kube/config
    echo

    echo
    echo "  -->>  Finalizando CONTROL en $(hostname): $0"
    echo
fi


if [ ${k8s_TIPO} == "work" ];
then
    echo "  -->>  Iniciando WORKER en $(hostname): $0"
    echo
    echo "Esperando los scripts de JOIN"
    while [ ! -f /vagrant/init.unlock ];
    do 
        sleep 10s
        scp -o 'StrictHostKeyChecking no' $(grep '## Control panel' /etc/hosts |grep  01| awk '{print $3}'):/vagrant/init.unlock  /vagrant/
        echo -n '.'
    done
    scp -o 'StrictHostKeyChecking no' $(grep '## Control panel' /etc/hosts |grep  01| awk '{print $3}'):/vagrant/add_{ctrl,worker}.sh /vagrant/
    echo "Ejecutando los scripts de JOIN"
    bash /vagrant/add_worker.sh
    echo
  
    echo "  -->>  Finalizando WORKER en $(hostname): $0"
fi
