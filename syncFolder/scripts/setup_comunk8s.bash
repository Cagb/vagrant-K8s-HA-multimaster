#!/bin/bash

[ -z $1 ] && echo "No se ha proporcionado un nombre de nodo" && exit 128
k8s_NODO=$1

[ -z $2 ] && echo "No se ha proporcionado un tipo de nodo" && exit 128
k8s_TIPO=$2

[ -z $3 ] && echo "No se ha proporcionado un numero de nodo" && exit 128
k8s_NUM=$3

. /vagrant/variablesEntorno.bash

#[ ${k8s_NODO} != $H ] && echo "Nombre de nodo incorrecto: $H y se esperaba ${k8s_NODO}" && exit 128
[ ${k8s_TIPO} != $T ] && echo "Tipo de nodo incorrecto: $T y se esperaba ${k8s_TIPO}" && exit 128
[ ${k8s_NUM} -ne $N ] && echo "Numero de nodo incorrecto: $N y se esperaba ${k8s_NUM}" && exit 128


echo "  -->>    -->>  Iniciando COMUN_K8S en $(hostname): $0"
echo

echo "  -->>  Instalando herramientas NFS"
DEBIAN_FRONTEND=noninteractive apt-get install -y nfs-common libnfs-utils nfs4-acl-tools
echo

echo "  -->>  Instalando y configurando Docker"
##apt-get install -y apt-transport-https ca-certificates curl software-properties-common
##echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" > /etc/apt/sources.list.d/dockerce.list
##curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
##apt-get update
##apt-get install -y containerd.io="${containerd_v}" docker-ce="${dockerce_ver}" docker-ce-cli="${dockercecli_ver}"

DEBIAN_FRONTEND=noninteractive apt-get install -y docker.io containerd
cat > /etc/docker/daemon.json <<EOF
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "log-driver": "json-file",
    "log-opts": {
        "max-size": "100m"
    },
    "storage-driver": "overlay2",
    "storage-opts": [
        "overlay2.override_kernel_check=true"
    ]
}
EOF
echo

echo "  -->>  Configurando Repositorio Kubernetes"
DEBIAN_FRONTEND=noninteractive apt-get install -y curl apt-transport-https gnupg
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list
echo

echo "  -->>  Instalando kubeadm, kubelet y kubectl"
DEBIAN_FRONTEND=noninteractive apt-get update
version=$(apt-cache madison kubeadm |grep "${k8s_v}" |sort -k2r |head -1 |awk '{print $3}')
DEBIAN_FRONTEND=noninteractive apt-get install -y kubeadm=${version} kubectl=${version} kubelet=${version}
apt-mark hold kubeadm kubelet kubectl 
echo

echo "  -->>  Letting iptables see bridged traffic"
echo "br_netfilter" > /etc/modules-load.d/k8s.conf
echo "net.bridge.bridge-nf-call-ip6tables = 1" > /etc/sysctl.d/k8s.conf
echo "net.bridge.bridge-nf-call-iptables = 1" >> /etc/sysctl.d/k8s.conf
sysctl --system
echo

echo "  -->>    -->>  Finalizando COMUN_K8S en $(hostname): $0"

