#!/bin/bash

[ -z $1 ] && echo "No se ha proporcionado un nombre de nodo" && exit 128
k8s_NODO=$1

[ -z $2 ] && echo "No se ha proporcionado un tipo de nodo" && exit 128
k8s_TIPO=$2

[ -z $3 ] && echo "No se ha proporcionado un numero de nodo" && exit 128
k8s_NUM=$3

. /vagrant/variablesEntorno.bash

#[ ${k8s_NODO} != $H ] && echo "Nombre de nodo incorrecto: $H y se esperaba ${k8s_NODO}" && exit 128
[ ${k8s_TIPO} != $T ] && echo "Tipo de nodo incorrecto: $T y se esperaba ${k8s_TIPO}" && exit 128
[ ${k8s_NUM} -ne $N ] && echo "Numero de nodo incorrecto: $N y se esperaba ${k8s_NUM}" && exit 128


echo "  -->>    -->>  Iniciando CONTROL en $(hostname): $0"
echo
kubeadm config images pull --kubernetes-version "${k8s_v}"
if [ ${k8s_NUM} -eq 01 ];
then
    echo "  -->>    -->>  Principal"
    echo "  -->>  Creando control-panel 01"
    kubeadm init \
        --kubernetes-version "${k8s_v}" \
        --control-plane-endpoint "api.k8s.cagb.es:6443" \
        --pod-network-cidr "10.128.0.0/9" \
        --service-dns-domain "apps.k8s.cagb.es" \
        --upload-certs \
        | tee /vagrant/k8s-init.txt
        #--dry-run \
        #--apiserver-bind-port 56443 \
        #--ignore-preflight-errors=Port-6443 \
        #--v=5
    echo "  -->>  Generando comandos para JOIN"
    tail -2 /vagrant/k8s-init.txt > /vagrant/add_worker.sh
    tail -12 /vagrant/k8s-init.txt | head -3 > /vagrant/add_ctrl.sh
    touch /vagrant/init.unlock
    echo
 
else
    echo "  -->>    -->>  Secundario"
    echo "  -->>  Esperando los scripts de JOIN"
    while [ ! -f /vagrant/init.unlock ];
    do 
        sleep 10s
        scp -o 'StrictHostKeyChecking no' $(grep '## Control panel' /etc/hosts |grep  01| awk '{print $3}'):/vagrant/init.unlock  /vagrant/
        echo -n '.'
    done
    scp -o 'StrictHostKeyChecking no' $(grep '## Control panel' /etc/hosts |grep  01| awk '{print $3}'):/vagrant/add_{ctrl,worker}.sh /vagrant/
    echo "  -->>  Ejecutando los scripts de JOIN"
    bash /vagrant/add_ctrl.sh
    echo
    
fi
echo "  -->>  Copiando fichero CONFIG al usuario ${K8S_USER}"
mkdir -p /home/${K8S_USER}/.kube
cp /etc/kubernetes/admin.conf /home/${K8S_USER}/.kube/config
chown ${K8S_USER}:${K8S_USER} /home/${K8S_USER}/.kube/config
echo

echo
echo "  -->>    -->>  Finalizando CONTROL en $(hostname): $0"
echo

