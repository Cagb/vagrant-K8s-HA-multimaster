#!/bin/bash

[ -z $1 ] && echo "No se ha proporcionado un nombre de nodo" && exit 128
k8s_NODO=$1

[ -z $2 ] && echo "No se ha proporcionado un tipo de nodo" && exit 128
k8s_TIPO=$2

[ -z $3 ] && echo "No se ha proporcionado un numero de nodo" && exit 128
k8s_NUM=$3

. /vagrant/variablesEntorno.bash

#[ ${k8s_NODO} != $H ] && echo "Nombre de nodo incorrecto: $H y se esperaba ${k8s_NODO}" && exit 128
[ ${k8s_TIPO} != $T ] && echo "Tipo de nodo incorrecto: $T y se esperaba ${k8s_TIPO}" && exit 128
[ ${k8s_NUM} -ne $N ] && echo "Numero de nodo incorrecto: $N y se esperaba ${k8s_NUM}" && exit 128


# Post-instalcion en el nodo de control principal
## Si no estamos en el nodo de control,no tiene sentido continuar
[ ${k8s_TIPO} != "ctrl" ] && echo "No se trata de un nodo de control: ${k8s_TIPO} y se esperaba \"ctrl\"" && exit 128
[ ${k8s_NUM} -ne 01 ]     && echo "No se trata del nodo de control 01: ${k8s_NUM} y se esperaba \"01\"" && exit 128

echo "  -->>    -->>  Iniciando POST-INSTALL en $(hostname): $0"
echo

echo "  -->>  Instalando CNI: calico"
#kubectl apply -f "https://docs.projectcalico.org/manifests/calico.yaml"
curl "https://docs.projectcalico.org/manifests/calico.yaml" | \
   sed 's#apiVersion: policy/v1beta1#apiVersion: policy/v1#' | \
   kubectl apply -f -

kubectl scale --replicas=$(kubectl get nodes | tail +2|wc -l) deployment/coredns -n kube-system
echo

echo "  -->>  Instalando Storage: NFS"
## Lo primero será preparar el PersistentVolume donde se indicará la ruta hacia el servidor, la ruta y el tamaño del storage, nfs-pv.yaml:
# Set the subject of the RBAC objects to the current namespace where the provisioner is being deployed
NS=$(kubectl config get-contexts|grep -e "^\*" |awk '{print $5}')
NAMESPACE=${NS:-default}
sed "s/namespace:.*/namespace: $NAMESPACE/g" /vagrant/nfs/rbac.yaml | kubectl create -f -

NFS_SRV="almc-01"
NFS_PATH="/datadisk01/storage1"
NFS_TAM_GiB=120
sed "s/namespace:.*/namespace: $NAMESPACE/g" /vagrant/nfs/deployment.yaml | \
  sed "s/10.3.243.101/${NFS_SRV}/g" | \
  sed "s#/ifs/kubernetes#${NFS_PATH}#g" | \
  kubectl create -f -

kubectl create -f /vagrant/nfs/class.yaml
echo

echo "  -->>  Instalando MetalLB"
kubectl get configmap kube-proxy -n kube-system -o yaml | sed -e "s/strictARP: false/strictARP: true/" | kubectl apply -n kube-system -f -
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.11.0/manifests/namespace.yaml
kubectl apply -f - <<EoF
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 10.9.9.161-10.9.9.163
EoF
curl https://raw.githubusercontent.com/metallb/metallb/v0.11.0/manifests/metallb.yaml | sed 's#apiVersion: policy/v1beta1#apiVersion: policy/v1#' | kubectl apply -f -
echo


echo "  -->>  Instalando Istio"
curl -L https://istio.io/downloadIstio | ISTIO_VERSION=${istio_v} TARGET_ARCH=x86_64 sh -
cd istio-${istio_v}
export PATH=$PWD/bin:$PATH
istioctl install --set profile=default -y
kubectl label namespace default istio-injection=enabled
istioctl analyze
##Deploy the sample application
echo
#echo "  -->>  Instalando Istio -- Deploy the sample application"
#kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml
#kubectl get services
#kubectl get pods
#sleep 15s
#watch kubectl get pods
#kubectl exec "$(kubectl get pod -l app=ratings -o jsonpath='{.items[0].metadata.name}')" -c ratings -- curl -sS productpage:9080/productpage | grep -o "<title>.*</title>"
#kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml
#istioctl analyze
#kubectl get svc istio-ingressgateway -n istio-system
#export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
#export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].port}')
#export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].port}')

## export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')
echo

exit
#echo "  -->>  Instalando Nginx Ingress Controler"
#kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.0/deploy/static/provider/cloud/deploy.yaml
#echo


# echo "  -->>  Instalando Egress Controler"
# curl https://raw.githubusercontent.com/nirmata/kube-static-egress-ip/master/config/static-egressip-crd.yaml | \
#   kubectl apply -f -
#
# curl https://raw.githubusercontent.com/nirmata/kube-static-egress-ip/master/config/static-egressip-rbac.yaml | \
#   kubectl apply -f -
#
# curl https://raw.githubusercontent.com/nirmata/kube-static-egress-ip/master/config/static-egressip-gateway-manager.yaml | \
#   sed "s/replicas: 3/replicas: 6/" | \
#   kubectl apply -f -
#
# curl https://raw.githubusercontent.com/nirmata/kube-static-egress-ip/master/config/static-egressip-controller.yaml | \
#   kubectl apply -f -
# ##Warning: spec.template.metadata.annotations[scheduler.alpha.kubernetes.io/critical-pod]: non-functional in v1.16+; use the "priorityClassName" field instead
#
# cat <<EOF | kubectl apply -f -
# apiVersion: staticegressips.nirmata.io/v1alpha1
# kind: StaticEgressIP
# metadata:
#   name: egress-mss
#   namespace: kube-system
# spec:
#   rules:
#   - egressip: 10.9.9.165
#     service-name: kube-dns
#     cidr: 10.9.9.0/24
# EOF
# echo

echo "  -->>  Instalando dashboard"
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.4.0/aio/deploy/recommended.yaml
kubectl create serviceaccount dashboard-admin-sa -n kubernetes-dashboard
kubectl create clusterrolebinding dashboard-admin-clb --clusterrole=cluster-admin --serviceaccount=kubernetes-dashboard:dashboard-admin-sa
TOKEN=$(kubectl get secret -n kubernetes-dashboard | grep dashboard-admin-sa | awk '{print $1}')
## hay que poner el token obtenido en la salida anterior.
kubectl describe secret "${TOKEN}" -n kubernetes-dashboard
kubectl apply -f - <<EoF
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: dashboard-ingress
  annotations:
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
  namespace: kubernetes-dashboard
spec:
  tls:
    - hosts:
       - dashboard.k8s.cagb.es
  rules:
  - host: dashboard.k8s.cagb.es
    http:
      paths:
        - backend:
            serviceName: kubernetes-dashboard
            servicePort: 443
EoF
echo

apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: dashboard-gateway
spec:
  selector:
    istio: ingressgateway # use istio default controller
  servers:
  - port:
      number: 443
      name: https
      protocol: HTTPS
    hosts:
    - "*"
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: dashboard
spec:
  hosts:
  - "*"
  gateways:
  - dashboard-gateway
  http:
  - match:
    - uri:
        exact: /dashboard
    route:
    - destination:
        host: kubernetes-dashboard
        port:
          number: 8443



echo "  -->>    -->>  Finalizando LoadBalancer en $(hostname): $0"
echo
