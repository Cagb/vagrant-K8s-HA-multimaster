#!/bin/bash

[ -z $1 ] && echo "No se ha proporcionado un nombre de nodo" && exit 128
k8s_NODO=$1

[ -z $2 ] && echo "No se ha proporcionado un tipo de nodo" && exit 128
k8s_TIPO=$2

[ -z $3 ] && echo "No se ha proporcionado un numero de nodo" && exit 128
k8s_NUM=$3

. /vagrant/variablesEntorno.bash

#[ ${k8s_NODO} != $H ] && echo "Nombre de nodo incorrecto: $H y se esperaba ${k8s_NODO}" && exit 128
[ ${k8s_TIPO} != $T ] && echo "Tipo de nodo incorrecto: $T y se esperaba ${k8s_TIPO}" && exit 128
[ ${k8s_NUM} -ne $N ] && echo "Numero de nodo incorrecto: $N y se esperaba ${k8s_NUM}" && exit 128


echo "  -->>    -->>  Iniciando WORKER en $(hostname): $0"
echo
echo "  -->>  Esperando los scripts de JOIN"
while [ ! -f /vagrant/init.unlock ];
do 
    sleep 10s
    scp -o 'StrictHostKeyChecking no' $(grep '## Control panel' /etc/hosts |grep  01| awk '{print $3}'):/vagrant/init.unlock  /vagrant/
    echo -n '.'
done
scp -o 'StrictHostKeyChecking no' $(grep '## Control panel' /etc/hosts |grep  01| awk '{print $3}'):/vagrant/add_{ctrl,worker}.sh /vagrant/
echo "  -->>  Ejecutando los scripts de JOIN"
bash /vagrant/add_worker.sh
echo

echo "  -->>    -->>  Finalizando WORKER en $(hostname): $0"

