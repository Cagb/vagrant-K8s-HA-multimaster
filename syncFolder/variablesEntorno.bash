#!/bin/bash

H=`hostname`
T=${H:0:4}
N=${H:5:2}

K8S_USER=k8sadmin

containerd_v="1.2.13-2"
dockerce_ver="5:19.03.11~3-0~ubuntu-$(lsb_release -cs)"
dockercecli_ver="5:19.03.11~3-0~ubuntu-$(lsb_release -cs)"

# 1.18.20
# 1.22.4
k8s_v="1.22.5"
istio_v="1.12.1"